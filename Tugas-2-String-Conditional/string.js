//Nomor 1
var word = 'JavaScript '
var second = 'is '
var third = 'awesome '
var fourth = 'and '
var fifth = 'I '
var sixth = 'love '
var seventh = 'it!'
console.log('1. ' + word.concat(second).concat(third).concat(fourth).concat(fifth).concat(sixth).concat(seventh))

//Nomor 2
var sentence = "I am going to be React Native Developer"

var firstWord = sentence[0]
var secondWord = sentence[2] + sentence[3]
var thirdWord = sentence.substr(5, 5)
var fourthWord = sentence[11] + sentence[12]
var fifthWord = sentence[14] + sentence[15]
var sixthWord = sentence.substr(17, 5)
var seventhWord = sentence.substr(23, 6)
var eighthWord = sentence.substr(30)

console.log('2.' + 'First Word: ' + firstWord)
console.log('Second Word: ' + secondWord)
console.log('Third Word: ' + thirdWord)
console.log('Fourth Word: ' + fourthWord)
console.log('Fifth Word: ' + fifthWord)
console.log('Sixth Word: ' + sixthWord)
console.log('Seventh Word: ' + seventhWord)
console.log('Eighth Word: ' + eighthWord)

//Nomor 3
var sentence2 = 'wow JavaScript is so cool'

var firstWord2 = sentence2.substr(0, 3)
var secondWord2 = sentence2.substr(4, 10)
var thirdWord2 = sentence2.substr(15, 2)
var fourthWord2 = sentence2.substr(18, 2)
var fifthWord2 = sentence2.substr(21, 4)

console.log('3. ' + 'First Word: ' + firstWord2)
console.log('Second Word: ' + secondWord2)
console.log('Third Word: ' + thirdWord2)
console.log('Fourth Word: ' + fourthWord2)
console.log('Fifth Word: ' + fifthWord2)

//Nomor 4
var sentence3 = 'wow JavaScript is so cool'

var firstWord3 = sentence3.substr(0, 3)
var secondWord3 = sentence3.substr(4, 10)
var thirdWord3 = sentence3.substr(15, 2)
var fourthWord3 = sentence3.substr(18, 2)
var fifthWord3 = sentence3.substr(21, 4)

var firstWordLength = firstWord3.length
var secondWordLength = secondWord3.length
var thirdWordLength = thirdWord3.length
var fourthWordLength = fourthWord3.length
var fifthWordLength = fifthWord3.length

console.log('4. ' + 'First Word: ' + firstWord3 + ', with length: ' + firstWordLength)
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength)
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength)
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength)
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength)

