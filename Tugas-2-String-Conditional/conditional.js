//if-else
var prompt = require('prompt-sync')()

var nama = prompt("Nama:")
var peran = prompt("Peran:")

if (nama == "" && peran == "" ) {
	console.log("Nama harus diisi!")
} else if (nama == "John" && peran == "") {
	console.log("Halo John, pilih peran mu untuk memulai game!")
} else if (nama == "Jane" && peran == "Penyihir") {
	console.log("Selamat datang di Dunia Werewolf, Jane")
	console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == "Jenita" && peran == "Guard") {
	console.log("Selamat datang di Dunia Werewofl, Jenita")
	console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == "junaedi" && peran == "Werewolf") {
	console.log("Selamat datang di Dunia Werewolf, Junaedi")
	console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
}

//Switch Case
var tanggal = prompt("Tanggal:")
var dd = ""
switch(tanggal){
	case "1":
		dd = console.log(1 );
		break;
	case "2":
		dd = console.log(2 );
		break;
	case "3":
		dd = console.log(3 );
		break;
	//dst
}

var bulan = prompt("Bulan:")
var mm = ""
switch(bulan){
	case "1":
		mm = console.log("Januari ");
		break;
	case "2":
		mm = console.log("Februari ");
		break;
	case "3":
		mm = console.log("Maret ");
		break;
	case "4":
		mm = console.log("April ");
		break;
	case "5":
		mm = console.log("Mei ");
		break;
	//dst
}

var tahun = prompt("Tahun:")
var yyyy = ""
switch(tahun){
	case "1":
		yyyy = console.log(1944);
		break;
	case "2":
		yyyy = console.log(1945);
		break;
	case "3":
		yyyy = console.log(1946);
		break;
	//dst
}

console.log(tanggal + "" + bulan + "" + tahun)
